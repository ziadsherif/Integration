const express = require("express");
const app = express();
const bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const users = [
  { username: "user1", password: "pass1" },
  { username: "user2", password: "pass2" },
];

app.post("/login", (req, res) => {
  const { username, password } = req.body;

  const user = users.find(
    (user) => user.username === username && user.password === password
  );

  if (user) {
    res.status(200).json({ message: "Login successful" });
    console.log("================================");
    console.log(user);
    console.log("================================");
  } else {
    console.log("--------------------------------");
    console.log(req.body);
    console.log("--------------------------------");
    res.status(401).json({ message: "Invalid credentials" });
  }
});

app.listen(3001, () => {
  console.log("Microservice is running on port 3001");
});