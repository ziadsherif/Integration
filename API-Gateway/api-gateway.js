const express = require("express");
const app = express();
app.use(express.json());
const axios = require("axios");
const cors = require("cors");

app.use(cors());
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, DELETE, OPTIONS"
  );
  next();
});

const userMicroserviceBaseUrl = "http://localhost:3001";

// verify authentication token
const verifyToken = (req, res, next) => {
  const token = req.headers.authorization;
  if (!token) {
    return res.status(401).json({ message: "Unauthorized" });
  }

  // implement the logic to verify the token here

  // If token is valid, allow the request to proceed
  next();
};

// Protected route that requires authentication
app.get("/api/protected", verifyToken, async (req, res) => {
  // Handle the protected route logic here
  res.status(200).json({ message: "You have access to this protected route" });
});

app.post("/api/login", async (req, res) => {
  try {
    const response = await axios.post(
      `${userMicroserviceBaseUrl}/login`,
      req.body
    );

    // Assuming the authentication service returns a valid token
    const token = response.data.token;

    console.log(req.body);

    res.status(response.status).json({ token }); // Send the token to the frontend
  } catch (error) {
    res.status(error.response.status).json(error.response.data);
  }
});

app.listen(8080, () => {
  console.log("API Gateway is running on port 8080");
});