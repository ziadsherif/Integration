const express = require('express');
const app = express();
const port = "8002"

app.use(express.json())

const Amadeus = require('amadeus');
const amadeus = new Amadeus({
    clientId: 'kUG33HkWyz8nAcusFgYKsiPMGAkU7hPu',
    clientSecret: 'CqOvvOAl3z87SZKJ',
});

app.get(`/city-and-airport-search/:parameter`, (req, res) => {
    const parameter = req.params.parameter;

    // Which cities or airports start with the parameter variable
    amadeus.referenceData.locations
        .get({
            keyword: parameter,
            subType:  "AIRPORT",
        
        })
        .then(function (response) {
            res.send(response.result);
        })
        .catch(function (response) {
            res.send(response);
        });
});


app.get(`/hotel`, (req, res) => {
  
    const {city} = req.query;

    
    // Which cities or airports start with the parameter variable
    amadeus.referenceData.locations.hotels.byCity
        .get({
            cityCode : city,  
        })
        .then(function (response) {
            res.send(response.result);
        })
        .catch(function (response) {
            res.send(response);
        });
});

app.listen(port, () => {
    console.log(`Third Party Gateway has started on port ${port}`);
  })
