const express = require('express');
const Booking= require('./Routes/Booking');
const axios =require('axios')
const app = express();
const port = "8000"

app.use(express.json())

app.use('/hotel',Booking)

app.listen(port, () => {
    console.log(`Gateway1 has started on port ${port}`);
  })
