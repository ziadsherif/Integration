const axios = require('axios')

const thirdPartyGatewayURL = "http://localhost:8002"
const getHotels = async(req,res) => {
     const {city} = req.params
     let cancel
     axios({
          method:"GET",
          url : `${thirdPartyGatewayURL}/hotel`,
          params : {city:city},
          cancelToken: new axios.CancelToken (c => cancel = c)
     }).then (response => {
          res.status(200).send(response.data)
     }).catch(e=>{
          res.status(400).send(e)
     })
     return () => cancel ()

     }




module.exports = {getHotels}