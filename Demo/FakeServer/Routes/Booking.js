const express = require ('express')
const router = express.Router()

const {getHotels} = require('../Controller/Booking')

router.get('/search/:city',getHotels)


module.exports = router 