const express = require('express');
const axios =require('axios')
const app = express();
const port = "8001"


const Booking = require('./Routes/Booking')

app.use('/api/hotel',Booking)
app.use(express.json())


app.listen(port, () => {
    console.log(`Fake Server has started on port ${port}`);
  })