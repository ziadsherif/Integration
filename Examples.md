```html
<!DOCTYPE html>
<html>
<head>
    <title>Healthcare Appointment Booking</title>
</head>
<body>
    <h1>Healthcare Appointment Booking</h1>

    <!-- Display Patient's Medical History -->
    <h2>Medical History</h2>
    <div id="medical-history"></div>

    <!-- Book Appointment Form -->
    <h2>Book an Appointment</h2>
    <form id="appointment-form">
        <label for="doctor-id">Doctor ID:</label>
        <input type="text" id="doctor-id" required><br>
        <label for="appointment-date">Appointment Date and Time:</label>
        <input type="datetime-local" id="appointment-date" required><br>
        <button type="submit">Book Appointment</button>
    </form>

    <script>
        const medicalHistoryContainer = document.getElementById('medical-history');
        const appointmentForm = document.getElementById('appointment-form');
        const doctorIdInput = document.getElementById('doctor-id');
        const appointmentDateInput = document.getElementById('appointment-date');

        // Fetch and display patient's medical history
        fetch('/api/patient/history/123') // Replace with actual patient ID
            .then(response => response.json())
            .then(data => {
                const history = data.history.join(', ');
                medicalHistoryContainer.textContent = `Medical History: ${history}`;
            });

        // Handle appointment booking form submission
        appointmentForm.addEventListener('submit', async (event) => {
            event.preventDefault();

            const doctorId = doctorIdInput.value;
            const appointmentDate = appointmentDateInput.value;

            const response = await fetch('/api/appointments/book', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    patientId: '123', // Replace with actual patient ID
                    doctorId: doctorId,
                    dateTime: appointmentDate,
                }),
            });

            if (response.ok) {
                alert('Appointment booked successfully!');
            } else {
                alert('An error occurred while booking the appointment.');
            }
        });
    </script>
</body>
</html>

```

```javascript
const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

// API endpoint to fetch patient's medical history
app.get('/api/patient/history/:patientId', (req, res) => {
    const patientId = req.params.patientId;
    // Call the patient history service to retrieve medical history
    // ...

    // Return medical history to the frontend
    const medicalHistory = { patientId: patientId, history: [...] };
    res.json(medicalHistory);
});

// API endpoint to book an appointment
app.post('/api/appointments/book', (req, res) => {
    const { patientId, doctorId, dateTime } = req.body;
    // Call the appointment booking service to schedule the appointment
    // ...

    // Return success response to the frontend
    res.json({ message: 'Appointment booked successfully.' });
});

app.listen(3000, () => {
    console.log('Integration API is running on port 3000');
});

```

```javascript
// Using Promises without async/await
function fetchData() {
    return fetch('https://api.example.com/data')
        .then(response => response.json())
        .then(data => {
            console.log(data);
        })
        .catch(error => {
            console.error('Error fetching data:', error);
        });
}

// Using async/await
async function fetchDataAsync() {
    try {
        const response = await fetch('https://api.example.com/data');
        const data = await response.json();
        console.log(data);
    } catch (error) {
        console.error('Error fetching data:', error);
    }
}

// Calling the functions
fetchData(); // Using Promises
fetchDataAsync(); // Using async/await

```